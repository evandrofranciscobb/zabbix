# Verificar se repositório "release" já está instalado
rpm -qa |grep zabbix

# Caso não exista, basta executar o comando abaixo
rpm -ivh http://repo.zabbix.com/zabbix/5.0/rhel/8/x86_64/zabbix-release-5.0-1.el8.noarch.rpm  
dnf clean all  
dnf update
dnf install -y zabbix-agent

# Vamos editar as configurações do agente
vim /etc/zabbix/zabbix_agentd.conf

# Procure pelos parametros abaixo e os substitua pelo valor correspondente
Server=127.0.0.1       # Por se tratar do Zabbix Server podemos manter o IP 127.0.0.1
ServerActive=127.0.0.1 # Por se tratar do Zabbix Server podemos manter o IP 127.0.0.1
Hostname=              # Define um nome para seu servidor a ser monitorado

# Iniciar o serviço e configurar a inicialização automática
systemctl enable --now zabbix-agent

# Verificar o status do serviço
systemctl status zabbix-agent

# Verificar o log
tail -f /var/log/zabbix/zabbix_agentd.log

# Validar o monitoramento do Zabbix Server no Frontend


